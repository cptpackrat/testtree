import { tmpdir } from 'os'
import {
  existsSync,
  readFileSync,
  readlinkSync,
  statSync
} from 'fs'
import {
  makeTestDir,
  makeTestRoot,
  makeTestTree,
  readTestTree,
  treeDir,
  treeFile,
  treeLink
} from '../src'

describe('makeTestDir', () => {
  const one = makeTestDir()
  const two = makeTestDir(one.path)
  it('makes test directories', () => {
    expect(statSync(one.path).isDirectory()).toBe(true)
    expect(statSync(two.path).isDirectory()).toBe(true)
    expect(one.path.startsWith(tmpdir())).toBe(true)
    expect(two.path.startsWith(one.path)).toBe(true)
  })
  describe('join', () => {
    it('makes joined paths', () => {
      const foo = one.join('foo')
      const bar = two.join('bar')
      expect(foo.startsWith(one.path)).toBe(true)
      expect(bar.startsWith(two.path)).toBe(true)
      expect(foo.endsWith('foo')).toBe(true)
      expect(bar.endsWith('bar')).toBe(true)
    })
  })
  describe('wipe', () => {
    it('deletes directories', () => {
      expect(existsSync(one.path)).toBe(true)
      expect(existsSync(two.path)).toBe(true)
      two.wipe()
      expect(existsSync(one.path)).toBe(true)
      expect(existsSync(two.path)).toBe(false)
      one.wipe()
      expect(existsSync(one.path)).toBe(false)
      expect(existsSync(two.path)).toBe(false)
    })
  })
  afterAll(() => {
    one.wipe()
    two.wipe()
  })
})

describe('makeTestTree', () => {
  const dir = makeTestTree({
    foo: treeDir({
      bar: treeFile('testing'),
      boo: treeDir()
    }),
    baz: treeLink('foo/bar')
  })
  it('makes test directory trees', () => {
    expect(statSync(dir.path).isDirectory()).toBe(true)
    expect(statSync(dir.join('foo')).isDirectory()).toBe(true)
    expect(statSync(dir.join('foo', 'bar')).isFile()).toBe(true)
    expect(statSync(dir.join('foo', 'boo')).isDirectory()).toBe(true)
    expect(statSync(dir.join('baz')).isFile()).toBe(true)
    expect(readlinkSync(dir.join('baz'))).toEqual('foo/bar')
    expect(readFileSync(dir.join('foo', 'bar'), 'utf8')).toEqual('testing')
    expect(readFileSync(dir.join('baz'), 'utf8')).toEqual('testing')
  })
  afterAll(() => dir.wipe())
})

describe('makeTestRoot', () => {
  const root = makeTestRoot()
  it('makes root test directories', () => {
    expect(statSync(root.path).isDirectory()).toBe(true)
    expect(root.path.startsWith(tmpdir())).toBe(true)
  })
  describe('makeTestDir', () => {
    it('makes test directories inside root', () => {
      const dir = root.makeTestDir()
      expect(statSync(dir.path).isDirectory()).toBe(true)
      expect(dir.path.startsWith(root.path)).toBe(true)
    })
  })
  describe('makeTestTree', () => {
    it('makes test directory trees inside root', () => {
      const tree = root.makeTestTree({
        foo: treeDir(),
        bar: treeFile('testing')
      })
      expect(statSync(tree.path).isDirectory()).toBe(true)
      expect(statSync(tree.join('foo')).isDirectory()).toBe(true)
      expect(statSync(tree.join('bar')).isFile()).toBe(true)
      expect(tree.path.startsWith(root.path)).toBe(true)
    })
  })
  afterAll(() => root.wipe())
})

describe('readTestTree', () => {
  const src = makeTestTree({
    foo: treeDir({
      bar: treeFile('testing'),
      boo: treeDir()
    }),
    baz: treeLink('foo/bar')
  })
  it('reads directory trees', () => {
    const dst = readTestTree(src.path)
    expect(dst.path).toEqual(src.path)
    expect(dst.tree).toStrictEqual(src.tree)
  })
  afterAll(() => src.wipe())
})

# @nfi/testtree
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]
[![standard-js][standard-image]][standard-url]

A set of utilities for creating and manipulating test directory structures.

## Installation
```
npm install @nfi/testtree
```

[npm-image]: https://img.shields.io/npm/v/@nfi/testtree.svg
[npm-url]: https://www.npmjs.com/package/@nfi/testtree
[pipeline-image]: https://gitlab.com/cptpackrat/testtree/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/testtree/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/testtree/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/testtree/commits/master
[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/

import { join } from 'path'
import { tmpdir } from 'os'
import {
  mkdirSync,
  mkdtempSync,
  readdirSync,
  readFileSync,
  readlinkSync,
  rmSync,
  symlinkSync,
  writeFileSync
} from 'fs'

export interface TestDir {
  path: string
  wipe: () => void
  join: (...args: string[]) => string
}

export function makeTestDir (root: string = tmpdir()): TestDir {
  const path = mkdtempSync(join(root, 'test-'))
  return {
    path,
    wipe: () => rmSync(path, { recursive: true, force: true }),
    join: (...args: string[]) => join(path, ...args)
  }
}

export interface TestRoot extends TestDir {
  makeTestDir: () => TestDir
  makeTestTree: (tree?: Tree) => TestTree
}

export function makeTestRoot (root?: string): TestRoot {
  const dir = makeTestDir(root)
  return {
    ...dir,
    makeTestDir: () => makeTestDir(dir.path),
    makeTestTree: (tree?: Tree) => makeTestTree(tree, dir.path)
  }
}

export interface TestTree extends TestDir {
  tree: Tree
}

export function makeTestTree (tree: Tree = {}, root?: string): TestTree {
  const dir = makeTestDir(root)
  _makeTestTree(dir.path, tree)
  return { ...dir, tree }
}

function _makeTestTree (path: string, tree: Tree): void {
  for (const name in tree) {
    const full = join(path, name)
    const item = tree[name]
    if (item.type === 'file') {
      writeFileSync(full, item.data)
    } else if (item.type === 'link') {
      symlinkSync(item.path, full)
    } else {
      mkdirSync(full)
      _makeTestTree(full, item.tree)
    }
  }
}

export function readTestTree (path: string): TestTree {
  return {
    path,
    wipe: () => rmSync(path, { recursive: true, force: true }),
    join: (...args: string[]) => join(path, ...args),
    tree: readTree(path)
  }
}

export function readTree (path: string): Tree {
  const tree: Tree = {}
  for (const entry of readdirSync(path, { withFileTypes: true })) {
    const full = join(path, entry.name)
    if (entry.isDirectory()) {
      tree[entry.name] = {
        type: 'dir',
        tree: readTree(full)
      }
    } else if (entry.isSymbolicLink()) {
      tree[entry.name] = {
        type: 'link',
        path: readlinkSync(full)
      }
    } else {
      tree[entry.name] = {
        type: 'file',
        data: readFileSync(full, 'utf8')
      }
    }
  }
  return tree
}

export interface TreeDir {
  type: 'dir'
  tree: Tree
}

export interface TreeFile {
  type: 'file'
  data: string
}

export interface TreeLink {
  type: 'link'
  path: string
}

export interface Tree {
  [index: string]: TreeDir | TreeFile | TreeLink
}

export function treeDir (tree: Tree = {}): TreeDir {
  return { type: 'dir', tree }
}

export function treeFile (data: string): TreeFile {
  return { type: 'file', data }
}

export function treeLink (path: string): TreeLink {
  return { type: 'link', path }
}
